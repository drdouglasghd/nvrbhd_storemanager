<?php

//require_once 'abstract.php';

ini_set('display_errors', 1);
error_reporting(-1);
define('DS', DIRECTORY_SEPARATOR);

class NB_Shell_Extract 
//extends Mage_Shell_Abstract
{

    public $stdin = null;
    public $defaultExtPath = '/Users/geoff/Development/extensions/';

    public function __construct(){
      $this->_parseArgs();
    }

    public function run()
    {
        if ($this->getArg('help')) {
            $this->usageHelp();

            return;
        }
        
        //cp -r skin/adminhtml/default/default/sopw/ ~/Development/extensions/CD_Workflow/skin/adminhtml/default/default/sopw/

        $this->out('Extension Extracting Script:');
        $this->out("Let's Get Setup:");
        
        if(!$this->getArg('ext'))
          $extDir = $this->in("What's is the Extension dir: \n(~/Development/extensions/CD_Workflow/)\nOR simply (CD_Workflow)");
        else 
          $extDir = $this->getArg('ext');
        if(strpos($extDir,'/') === false){
          $extDir = $this->defaultExtPath . $extDir . DS;
        }

        $mode = $this->in("Which direction are we going?: ([in] or [out])",array('in','out','x'),'out');
        if($mode == 'x') exit;

        $this->out("Now we are going in to a resource loop, hit x to exit.");
        $resource = '';
        while($resource != 'x'){

          // Display Resources
          $configs = $this->getExtensionConfig($extDir);
          if(!isset($configs['symlinks'])){
            $this->out($extDir);
            $this->out('No Configs... check Extension dir!'); break;
          }
          $symlinks = array_map(function($item){
            if(is_file($item['value']))
              return $item['value'];
            if(is_dir($item['value']))
              return $item['value'].'/';
            return $item['value'];

          },$configs['symlinks']); 
          print_r($symlinks);

          // Choose IN or OUT, are we moving the extension into the store, or out into the extensions directory
          switch($mode){
            case 'in':
              // $cmd = sprintf('ls %s',$extDir);
              // $output = array();
              // exec("$cmd 2>&1", $output, $exit_status);
              // print_r($output);
              // $resource = $this->in('folder from the ext to copy to store: (app)');
              // if($resource == 'x' || !$resource) break;

              $siteRoot = realpath(dirname(__FILE__) . '/../') . '/';

              // $this->out('You entered: ' . $resource);
              // $this->copyResources($extDir . $resource,$siteRoot);
              // break;

              $resource = $this->in('Relative Path to resource: (skin/adminhtml/default/default/sopw/)');
              if($resource == 'x' || $resource == '' || $resource === false) break;
              if(isset($symlinks[$resource])) $resource = $symlinks[$resource];
              if($resource == 'all'){
                foreach ($symlinks as $key => $value) {
                  //$this->out('You entered: key: ' . $key . ',' . $value);
                  // $this->copyResources($value,$extDir.$value);
                  $this->copyResourcesIn($extDir.$value,$siteRoot.$value);
                }
              } elseif(in_array($resource,$symlinks)){
                $this->out('You entered: ' . $resource);
                $this->copyResourcesIn($extDir.$resource,$siteRoot.$resource);
              }
              break;


            case 'out':
              // List out the paths from the install.xml
              $this->out($extDir);
    
              $resource = $this->in('Relative Path to resource: (skin/adminhtml/default/default/sopw/)');
              if($resource == 'x' || $resource == '' || $resource === false) break;
              if(isset($symlinks[$resource])) $resource = $symlinks[$resource];
              if($resource == 'all'){
                foreach ($symlinks as $key => $value) {
                  //$this->out('You entered: key: ' . $key . ',' . $value);
                  $this->copyResourcesOut($value,$extDir.$value);
                }
              } elseif(in_array($resource,$symlinks)){
                $this->out('You entered: ' . $resource);
                $this->copyResourcesOut($resource,$extDir.$resource);
              }
              break;

            default:
              break;

          }
        }

    }

    public function copyResourcesIn($from,$to){
      $this->out('Copying ' . $from . ' to ' . $to . ':');
      if(is_dir($from)){
        $filepath = explode(DS,$to);
        array_pop($filepath);
        $copyto = implode(DS,$filepath);
        $cmd = sprintf('rm -rf %s && mkdir -p %s && cp -r %s %s',$to,$to,$from,$copyto);
        $this->out('Executing: ' . $cmd);
      } else {
        $filepath = explode(DS,$to);
        $filename = array_pop($filepath);
        $filedir = DS . implode(DS,$filepath);
        $cmd = sprintf('mkdir -p %s && cp -r %s %s',$filedir,$from,$to);
      }
      exec("$cmd 2>&1", $output, $exit_status);
      if(!empty($output)) print_r($output);
    }

    public function copyResourcesOut($from,$to){
      if($this->getArg('remove')) $rm = true; else $rm = false;
      $this->out('Copying ' . $from . ' to ' . $to . ':');
      if(is_dir($from)){
        $cmd = sprintf('rm -rf %s && mkdir -p %s && cp -r %s %s',$to,$to,$from,$to);
      } else {
        $filepath = explode(DS,$to);
        $extensionFolderCheck = array_slice($filepath,-3,1);
        if(end($extensionFolderCheck) == 'extensions') $rm = false;
        $filename = array_pop($filepath);
        $to = DS . implode(DS,$filepath);

        //echo 'TO: ' . DS.implode(DS,array_shift(explode(DS,$to)));
        $cmd = '';
        if($rm){
          $cmd .= sprintf('rm -rf %s &&',$to);
        }
        $cmd .= sprintf('mkdir -p %s && cp -r %s %s',$to,$from,$to);
      }
      exec("$cmd 2>&1", $output, $exit_status);
      if(!empty($output)) print_r($output);
    }

    /**
     * Extension config 
     * 
     * @param string $id e.g: "fixtures"
     */
    protected function getExtensionConfig($extDir)
    {

        $packageConfig = sprintf("%sinstall.xml", $extDir);
        
        if (is_file($packageConfig)) {
            $data = array();
            $dom = new DOMDocument();
            $dom->loadXML(file_get_contents($packageConfig));
            $xpath = new DOMXPath($dom);
            $sections = $xpath->query('/config/*');
            
            foreach ($sections as $section) {
                /* @var $section DOMElement */
                if (!($section instanceof DOMElement)) {
                    continue;
                }
                $newData = array();
                foreach($section->getElementsByTagName('item') as $item) {
                    /* @var $item DOMElement */
                    $ignore = ($item->hasAttribute('ignore')) ? $item->getAttribute('ignore') : 'true' ;
                    $newData[] = array('value' => $item->nodeValue, 'ignore' => $ignore);
                }
                
                $data[$section->nodeName] = $newData;
            }
            
            return $data;
        }
        else {
            return $packageConfig . ': Not File! ERROR(125)';
        }
    }

    public function usageHelp()
    {
        return <<<USAGE
Usage:  php extract.php -- [options]

  --help                        This help

USAGE;
    }

    public function in($prompt, $options = null, $default = null) {
      if(!$this->stdin){
        $this->stdin = fopen('php://stdin', 'r');
      }

      $in = $this->getInput($prompt,$options,$default);

      if ($options && is_string($options)) {
        if (strpos($options, ',')) {
          $options = explode(',', $options);
        } elseif (strpos($options, '/')) {
          $options = explode('/', $options);
        } else {
          $options = array($options);
        }
      }
      if (is_array($options)) {
        while ($in === '' || ($in !== '' && (!in_array(strtolower($in), $options) && !in_array(strtoupper($in), $options)) && !in_array($in, $options))) {
          $in = $this->getInput($prompt,$options,$default);
        }
      }
      return $in;
    }

    function getInput($prompt, $options = null, $default = null) {
      if (!is_array($options)) {
        $printOptions = '';
      } else {
        $printOptions = '(' . implode('/', $options) . ')';
      }

      if ($default === null) {
        $this->out($prompt . " $printOptions \n" . '> ',false);
      } else {
        $this->out($prompt . " $printOptions \n" . "[$default] > ",false);
      }
      $result = fgets($this->stdin);

      if ($result === false) {
        exit;
      }
      $result = trim($result);

      if ($default !== null && ($result === '' || $result === null)) {
        return $default;
      }
      return $result;
    }

    public function out($message,$newLine = true){
      echo $message;
      if($newLine) echo "\n";
    }

    /**
     * Parse input arguments
     *
     * @return Mage_Shell_Abstract
     */
    protected function _parseArgs()
    {
        $current = null;
        foreach ($_SERVER['argv'] as $arg) {
            $match = array();
            if (preg_match('#^--([\w\d_-]{1,})$#', $arg, $match) || preg_match('#^-([\w\d_]{1,})$#', $arg, $match)) {
                $current = $match[1];
                $this->_args[$current] = true;
            } else {
                if ($current) {
                    $this->_args[$current] = $arg;
                } else if (preg_match('#^([\w\d_]{1,})$#', $arg, $match)) {
                    $this->_args[$match[1]] = true;
                }
            }
        }
        return $this;
    }

    /**
     * Retrieve argument value by name or false
     *
     * @param string $name the argument name
     * @return mixed
     */
    public function getArg($name)
    {
        if (isset($this->_args[$name])) {
            return $this->_args[$name];
        }
        return false;
    }

}

$app = new NB_Shell_Extract();
$app->run();
