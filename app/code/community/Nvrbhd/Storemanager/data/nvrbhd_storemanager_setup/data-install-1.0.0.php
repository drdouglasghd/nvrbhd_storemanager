<?php
	
	$rolename = 'Store Manager Role';
	$aclresources = array(
		'__root__',
		'nvrbhd_storemanager',
		'nvrbhd_storemanager/api',
		'nvrbhd_storemanager/api/install',
		'nvrbhd_storemanager/api/manage',
		'nvrbhd_productmanager',
		'nvrbhd_productmanager/api',
		'nvrbhd_productmanager/api/import'
	);

    $rid  = false;
    $role = Mage::getModel('api/roles')->load($rid);

    $role = $role
            ->setName($rolename)
            ->setPid(false)
            ->setRoleType('G')
            ->save();

    Mage::getModel('api/rules')
        ->setRoleId($role->getId())
        ->setResources($aclresources)
        ->saveRel();

    $rid = $role->getId();

	$userapi = Mage::getModel('api/user')
		->setData(array(
			'username' => 'storemanager',
			'firstname' => 'Store',
			'lastname' => 'Manager',
			'email' => 'email@neverbehind.com',
			'api_key' => 'Bwq16CZi0pqWKmVNRuEy',
			'api_key_confirmation' => 'Bwq16CZi0pqWKmVNRuEy',
			'is_active' => 1,
			'user_roles' => '',
			'assigned_user_role' => '',
			'role_name' => '',
			'roles' => array($rid) // your created custom role
		));

	$userapi->save();

	$userapi->setRoleIds(array($rid))  // your created custom role
		->setRoleUserId($userapi->getUserId())
		->saveRelations();