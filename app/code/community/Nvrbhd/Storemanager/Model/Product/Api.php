<?php

/**
 * Product Management API
 *
 * @category   Nvrbhd
 * @package    Nvrbhd_Storemanager
 * @author     drdouglasghd
 */
class Nvrbhd_Storemanager_Model_Product_Api extends Mage_Api_Model_Resource_Abstract
{

    /**
     * Product Management API
     */

    /**
     * Import a Product into the Store
     *
     * @param array    // Data passed in to the API call
     *
     * @return array
     */
    public function import($data = array())
    {
      // Do Stuff
      return $data;
    }

}
