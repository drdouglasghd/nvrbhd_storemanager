<?php

/**
 * Store Management API
 *
 * @category   Nvrbhd
 * @package    Nvrbhd_Storemanager
 * @author     drdouglasghd
 */
class Nvrbhd_Storemanager_Model_Api extends Mage_Api_Model_Resource_Abstract
{

    /**
     * Management API
     */

    /**
     * Manage Something in the Store
     *
     * @param array    // Data passed in to the API call
     *
     * @return array
     */
    public function manage($data = array())
    {
      // Do Stuff
      return $data;
    }

    /**
     * Install Something in the Store
     *
     * @param array    // Data passed in to the API call
     *
     * @return array
     */
    public function install($data = array())
    {
      // Do Stuff
      return $data;
    }

}
