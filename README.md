# Simple Store Manager API Extension #

Simple API Shell

### What is this repository for? ###

* Contains Store Management API's
* 1.0.0

### How do I get set up? ###

* Copy Files into Magento Install

### Usage ###

SOAP
```
#!php

$client = new SoapClient('http://magentohost/soap/api/?wsdl');
$session = $client->login('apiUser', 'apiKey');
$result = $client->call($session, 'nvrbhd_storemanager.manage',array('key'=>'value'));
```

JSON (Requires NB_Jsonapi extension)

```
Endpoint: https://magentohost/json/api/call/

Additionally you can use the resource_path in url: /json/api/call/resource_path/nvrbhd_productmanager.import

// Payload:
{"user":"webservice-username","token":"webservice-token","resource_path":"nvrbhd_storemanager.manage","args":[{"key":"value"}]}

// Expect Results:
{"results":{"key":"value"}}

```

### Available Methods ###
* nvrbhd_storemanager.manage
* nvrbhd_storemanager.install
* nvrbhd_productmanager.import

### Default API User and Role ###
* Install creates 'storemanager' API/Webservices User and 'Store Manager Role' API/Webservices Role
* **Recommended** that the User API Key/Password is Changed by an Admin User

### Who do I talk to? ###

* drdouglasghd | geoff@neverbehind.com